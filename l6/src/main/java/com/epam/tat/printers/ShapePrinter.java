package com.epam.tat.printers;

import com.epam.tat.enums.ShapeEnum;
import com.epam.tat.shapes.Shape;

import java.util.List;

/**
 * Created by dverevkin on 7/17/2017.
 */
public class ShapePrinter {

    public static void printShape(ShapeEnum shapeEnum, String[] splitArgs) {
        if (splitArgs.length > 2) {
            System.out.println(shapeEnum.getName() + " - " + splitArgs[1] + ", " + splitArgs[2]);
        } else {
            System.out.println(shapeEnum.getName() + " - " + splitArgs[1]);
        }
    }

    public static void printShapes(List<Shape> allShapes) {
        for (Shape shape : allShapes) {
            System.out.println("Type:" + shape.getStringType());
            System.out.println("Area:" + shape.getArea());
            System.out.println("Perimeter:" + shape.getPerimeter());
            System.out.println();
        }
    }
}
