package com.epam.tat.shapes;

import com.epam.tat.enums.ShapeEnum;

/**
 * Created by dverevkin on 7/5/2017.
 */
public class Rectangle implements Shape {

    public static final int HASH_CODE_CONSTANT = 31;
    private int width;
    private int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public int getPerimeter() {
        return width * 2 + height * 2;
    }

    @Override
    public int getArea() {
        return width * height;
    }

    @Override
    public String getStringType() {
        return ShapeEnum.RECTANGLE.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Rectangle)) {
            return false;
        }

        Rectangle rectangle = (Rectangle) o;

        if (width != rectangle.width) {
            return false;
        }
        return height == rectangle.height;
    }

    @Override
    public int hashCode() {
        return HASH_CODE_CONSTANT * width + height;
    }
}
