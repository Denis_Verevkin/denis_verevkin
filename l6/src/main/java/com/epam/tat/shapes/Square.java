package com.epam.tat.shapes;

import com.epam.tat.enums.ShapeEnum;

/**
 * Created by dverevkin on 7/5/2017.
 */
public class Square implements Shape {

    private static final int COUNT_OF_SIDES = 4;

    private int size;

    public Square(int size) {
        this.size = size;
    }

    @Override
    public int getPerimeter() {
        return size * COUNT_OF_SIDES;
    }

    @Override
    public int getArea() {
        return size * size;
    }

    @Override
    public String getStringType() {
        return ShapeEnum.SQUARE.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Square)) {
            return false;
        }

        Square square = (Square) o;

        return size == square.size;
    }

    @Override
    public int hashCode() {
        return size;
    }
}
