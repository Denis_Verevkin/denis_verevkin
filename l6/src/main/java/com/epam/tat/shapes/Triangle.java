package com.epam.tat.shapes;

import com.epam.tat.enums.ShapeEnum;

/**
 * Created by dverevkin on 7/5/2017.
 */
public class Triangle implements Shape {

    private static final int DEGREES_60 = 60;

    private static final int COUNTS_OF_SIDES = 3;

    private static final double HALF = 0.5;

    private int size;

    public Triangle(int size) {
        this.size = size;
    }

    @Override
    public int getPerimeter() {
        return size * COUNTS_OF_SIDES;
    }

    @Override
    public int getArea() {
        return (int) (HALF * size * size * Math.sin(Math.toRadians(DEGREES_60)));
    }

    @Override
    public String getStringType() {
        return ShapeEnum.TRIANGLE.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Triangle)) {
            return false;
        }

        Triangle triangle = (Triangle) o;

        return size == triangle.size;
    }

    @Override
    public int hashCode() {
        return size;
    }
}
