package com.epam.tat.processors;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dverevkin on 7/17/2017.
 */
public class CommandLineProcessor {

    public static List<String> parseArgs(String[] args) {
        List<String> inputData = new ArrayList<>();
        for (int i = 1; i < args.length; i++) {
            inputData.add(args[i]);
        }
        return inputData;
    }
}
