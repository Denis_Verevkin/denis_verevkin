package com.epam.tat.processors;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by dverevkin on 7/17/2017.
 */
public class FileProcessor {

    public static List<String> parseFile(String fileName) {
        List<String> inputData = new ArrayList<>();
        File file = new File(fileName);
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                inputData.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("No such file.");
            e.printStackTrace();
        }
        return inputData;
    }
}
