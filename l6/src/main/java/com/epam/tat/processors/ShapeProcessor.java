package com.epam.tat.processors;

import com.epam.tat.enums.ShapeEnum;
import com.epam.tat.printers.ShapePrinter;
import com.epam.tat.shapes.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by dverevkin on 7/17/2017.
 */
public class ShapeProcessor {

    public static void process(List<String> args) throws Exception {
        Set<Shape> shapes = new HashSet<>();
        for (String arg : args) {
            String[] splitArgs = arg.split(":");
            ShapeEnum shapeEnum = ShapeEnum.getByName(splitArgs[0]);
            if (shapeEnum != null) {
                ShapePrinter.printShape(shapeEnum, splitArgs);
                shapes.add(getShape(shapeEnum, splitArgs));
            }
        }

        System.out.println("------------------------------------------");

        List<Shape> allShapes = new ArrayList<>(shapes);
        allShapes.sort((o1, o2) -> {
            int x = o1.getArea();
            int y = o2.getArea();
            if (x < y) {
                return 1;
            } else {
                if (x == y) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });

        ShapePrinter.printShapes(allShapes);
    }

    private static Shape getShape(ShapeEnum shapeEnum, String[] splitArgs) throws Exception {
        Shape shape;
        switch (shapeEnum) {
            case CIRCLE:
                shape = new Circle(Integer.parseInt(splitArgs[1]));
                break;
            case TRIANGLE:
                shape = new Triangle(Integer.parseInt(splitArgs[1]));
                break;
            case RECTANGLE:
                shape = new Rectangle(Integer.parseInt(splitArgs[1]), Integer.parseInt(splitArgs[2]));
                break;
            case SQUARE:
                shape = new Square(Integer.parseInt(splitArgs[1]));
                break;
            default:
                throw new Exception("Shape wasn't matched");
        }
        return shape;
    }
}
