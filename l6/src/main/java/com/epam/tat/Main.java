package com.epam.tat;

import com.epam.tat.processors.CommandLineProcessor;
import com.epam.tat.processors.FileProcessor;
import com.epam.tat.processors.ShapeProcessor;

import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        // triangle:3 circle:6 square:4 rectangle:5:6 square:16 triangle:5
        if (args == null || args.length <= 1) {
            System.out.println("Nothing to do.");
            return;
        }

        List<String> inputData = getInputData(args);
        ShapeProcessor.process(inputData);
    }

    private static List<String> getInputData(String[] args) throws Exception {
        if (args[0].equals("--plain")) {
            return CommandLineProcessor.parseArgs(args);
        } else if (args[0].equals("--path")) {
            return FileProcessor.parseFile(args[1]);
        }
        throw new Exception("Please, use --plain or --path command line arguments.");
    }
}
