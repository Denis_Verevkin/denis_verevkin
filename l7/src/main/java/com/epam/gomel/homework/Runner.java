package com.epam.gomel.homework;

public class Runner {

    public static void main(String[] args) {
        Boy boy = new Boy(Month.JANUARY, 10_000_000.0);
        boy.spendSomeMoney(50.0);
        Girl girl = new Girl(true, true, boy);
        girl.spendBoyFriendMoney(100.0);
    }
}
