package com.epam.gomel.homework;

import lombok.Getter;
import lombok.Setter;

public class Girl extends Human {

    @Setter
    @Getter
    private boolean pretty;
    @Getter
    @Setter
    private Boy boyfriend;
    @Getter
    private boolean slimFriendGotAFewKilos;

    public Girl(boolean pretty, boolean slimFriendGotAFewKilos, Boy boyfriend) {
        this.pretty = pretty;
        this.slimFriendGotAFewKilos = slimFriendGotAFewKilos;
        this.boyfriend = boyfriend;
        if (boyfriend != null) {
            this.boyfriend.setGirlfriend(this);
        }
    }

    public Girl(boolean pretty, boolean slimFriendGotAFewKilos) {
        this(pretty, slimFriendGotAFewKilos, null);
    }

    public Girl(boolean pretty) {
        this(pretty, false, null);
    }

    public Girl() {
        this(false, true, null);
    }

    @Override
    public Mood getMood() {
        if (isBoyFriendWillBuyNewShoes()) {
            return Mood.EXCELLENT;
        } else if (pretty || isBoyfriendRich()) {
            return Mood.GOOD;
        } else if (isSlimFriendBecameFat()) {
            return Mood.NEUTRAL;
        }
        return Mood.I_HATE_THEM_ALL;
    }


    public void spendBoyFriendMoney(double amountForSpending) {
        if (isBoyfriendRich()) {
            boyfriend.spendSomeMoney(amountForSpending);
        }
    }


    private boolean isBoyfriendRich() {
        return boyfriend != null && boyfriend.isRich();
    }

    private boolean isBoyFriendWillBuyNewShoes() {
        return isBoyfriendRich() && pretty;
    }

    private boolean isSlimFriendBecameFat() {
        return slimFriendGotAFewKilos;
    }

}
