package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by dverevkin on 7/23/2017.
 */
public class GirlTest {

    private Girl girl;

    @BeforeClass
    public void beforeClass() throws Exception {
        System.out.println("Fake class created/@Before annotation testing ");
        this.girl = new Girl(true);
    }

    @AfterClass
    public void afterClass() throws Exception {
        System.out.println("Fake class closed/@After annotation testing ");
        this.girl = null;
    }

    @DataProvider(name = "data-provider_mood")
    public Object[][] dataProviderArr_mood() {
        return new Object[][]{
                {true, true, false, Mood.EXCELLENT},
                {true, false, true, Mood.GOOD},
                {false, false, true, Mood.NEUTRAL},
                {false, false, false, Mood.I_HATE_THEM_ALL}

        };
    }

    @DataProvider(name = "data-provider_spendMoney")
    public Object[][] dataProviderArr_spendMoney() {
        return new Object[][] {
                {true, true, 100.0, null},
                {true, true, 10000000.0,"Not enough money! Requested amount is 1.0E7$ but you can't spend more then 1000000.0$" }

        };
    }

    @Test(dataProvider = "data-provider_spendMoney")
    public void testSpendBoyFriendMoney(boolean pretty, boolean slimFriendGotAFewKilos, double spentMoney, String errMessage ) {
        Girl girl = new Girl(pretty, slimFriendGotAFewKilos);
        Boy boy = new Boy(Month.JULY, 1000000.0, girl);

        try {
            girl.spendBoyFriendMoney(spentMoney);
            } catch (RuntimeException exceptionMoneyNotEnough) {
            Assert.assertNotNull(errMessage, "uytuygjhg");
            Assert.assertEquals(errMessage, exceptionMoneyNotEnough.getMessage());
        }
    }

    @Test(dataProvider = "data-provider_mood")
    public void testGetGirlMood(boolean isBoyReach, boolean pretty, boolean slimFriendGotAFewKilos, Mood expectedMood) {
        Girl girl = new Girl(pretty, slimFriendGotAFewKilos);
        Boy boy = mock(Boy.class);
        when(boy.isRich()).thenReturn(isBoyReach);
        girl.setBoyfriend(boy);
        Mood girlMoodActual = girl.getMood();
        Assert.assertEquals(girlMoodActual, expectedMood, "Invalid mood");
        System.out.println(girl);
    }

}