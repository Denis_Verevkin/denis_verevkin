package com.epam.gomel.homework;

import org.testng.annotations.Factory;
import org.testng.annotations.Parameters;

public class BoyTestFactory {
    
    @Factory
    public Object[] create() {
        return new Object[]{
                new BoyFactoryTest(Month.JANUARY, 1000000, true, false),
                new BoyFactoryTest(Month.JANUARY, 1000000, true, false)
        };
    }

}