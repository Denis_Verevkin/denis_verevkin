package com.epam.gomel.homework;

import org.testng.IExecutionListener;

public class ExecutionListener implements IExecutionListener {
    private long startTime;

	@Override
	public void onExecutionStart() {
		startTime = System.currentTimeMillis();
		System.out.println("TEST Listener Info!!! TestNG is going to start");
	}

	@Override
	public void onExecutionFinish() {
		System.out.println("TEST Listener Info!!! TestNG has finished, took around " + (System.currentTimeMillis() - startTime) + "ms");
	}
}