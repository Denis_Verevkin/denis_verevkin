package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

/**
 * Created by dverevkin on 7/28/2017.
 */
public class BoyFactoryTest {

    private Month birthdayMonth;

    private double wealth;

    private boolean pretty;

    private boolean slimFriendGotAFewKilos;

    public BoyFactoryTest() {
    }

    BoyFactoryTest(Month birthdayMonth, double wealth, boolean pretty, boolean slimFriendGotAFewKilos) {
        this.birthdayMonth = birthdayMonth;
        this.wealth = wealth;
        this.pretty = pretty;
        this.slimFriendGotAFewKilos = slimFriendGotAFewKilos;
    }

    @Test(description = "Testing of getting wealth")
    public void testGettingWealth() {
        Girl girlfriend = new Girl(pretty, slimFriendGotAFewKilos);
        Boy boy = new Boy(birthdayMonth, wealth, girlfriend);
        Assert.assertEquals(boy.getWealth(), wealth, "Incorrect wealth");
    }

}

