package com.epam.gomel.homework;

import org.hamcrest.MatcherAssert;
import org.testng.Assert;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created by dverevkin on 7/23/2017.
 */
public class BoyTest {

    private Boy boy;

    @BeforeClass
    public void beforeClass() throws Exception {
        this.boy = new Boy(Month.JANUARY);
    }

    @AfterClass
    public void afterClass() throws Exception {
        this.boy = null;
    }

    @DataProvider(name = "data-provider_summerMonth")
    public Object[][] dataProviderArr_summerMonth() {
        return new Object[][] {
                {Month.JULY, true},
                {Month.JANUARY, false},

        };
    }

    @DataProvider(name = "data-provider_rich")
    public Object[][] dataProviderArr_rich() {
        return new Object[][] {
                {Month.JULY, 1000000, true},
                {Month.JANUARY, 10, false},

        };
    }

    @DataProvider(name = "data-provider_mood")
    public Object[][] dataProviderArr_mood() {
        return new Object[][] {
                {Month.JULY, 1000000, true, false, Mood.EXCELLENT},
                {Month.JANUARY, 1000000, true, false, Mood.GOOD},
                {Month.JANUARY, 1000000, false, true, Mood.BAD},
                {Month.JULY, 1000000, false, true, Mood.NEUTRAL},
                {Month.JANUARY, 10, false, false, Mood.HORRIBLE}
        };
    }


    @Test(dataProvider = "data-provider_mood", dependsOnMethods = "testSpendSomeMoney")
    public void testGetBoyMood(Month birthdayMonth, double wealth, boolean pretty, boolean slimFriendGotAFewKilos, Mood expectedMood) {
        Girl girl = new Girl(pretty, slimFriendGotAFewKilos);
        Boy boy = new Boy(birthdayMonth, wealth, girl);
        Mood boyMoodActual = boy.getMood();
        assertThat(boyMoodActual, equalTo(boyMoodActual));
    }


    @Test(description = "Testing of spending money")
    @Parameters({"spentMoney_yes"})
    public void testSpendSomeMoney(double spentMoney_yes) {
        Boy boy = new Boy(Month.JANUARY, 200);
        boy.spendSomeMoney(spentMoney_yes);
        System.out.println("Spent money: " + spentMoney_yes);
        Assert.assertEquals(boy.getWealth(),   100.0, "Incorrect wealth");
    }

    @Test(dataProvider = "data-provider_summerMonth")
    public void testIsSummerMonth(Month birthdayMonth, Boolean expectedMonth) {
        Boy boy = new Boy(birthdayMonth);
        Boolean boyBirthdayMonth = boy.isSummerMonth();
        Assert.assertEquals(boyBirthdayMonth, expectedMonth, "Invalid month");
        System.out.println(boy);
    }

    @Test(dataProvider = "data-provider_rich")
    public void testIsRich(Month birthdayMonth, double wealth, Boolean expectedRich) {
        Boy boy = new Boy(birthdayMonth, wealth);
        Boolean boyRich = boy.isRich();
        Assert.assertEquals(boyRich, expectedRich, "Invalid month");
        System.out.println(boy);
    }
}
