package com.epam.tat.shapes;

/**
 * Created by dverevkin on 7/5/2017.
 */
public class Circle implements Shape {

    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public int perimeter() {
        return (int) (2 * Math.PI * radius);
    }

    @Override
    public int area() {
        return (int) (Math.PI * radius * radius);
    }
}
