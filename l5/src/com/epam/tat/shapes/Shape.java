package com.epam.tat.shapes;

/**
 * Created by dverevkin on 7/5/2017.
 */
public interface Shape {

    int perimeter();

    int area();
}
