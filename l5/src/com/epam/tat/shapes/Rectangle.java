package com.epam.tat.shapes;

/**
 * Created by dverevkin on 7/5/2017.
 */
public class Rectangle implements Shape {

    private int width;
    private int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public int perimeter() {
        return width * 2 + height * 2;
    }

    @Override
    public int area() {
        return width * height;
    }
}
