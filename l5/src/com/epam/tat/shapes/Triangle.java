package com.epam.tat.shapes;

/**
 * Created by dverevkin on 7/5/2017.
 */
public class Triangle implements Shape {

    private int size;

    public Triangle(int size) {
        this.size = size;
    }

    @Override
    public int perimeter() {
        return size * 3;
    }

    @Override
    public int area() {
        return (int) (0.5 * size * size * Math.sin(Math.toRadians(60)));
    }
}
