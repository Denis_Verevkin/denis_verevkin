package com.epam.tat.shapes;

/**
 * Created by dverevkin on 7/5/2017.
 */
public class Square implements Shape {

    private int size;

    public Square(int size) {
        this.size = size;
    }

    @Override
    public int perimeter() {
        return size * 4;
    }

    @Override
    public int area() {
        return size * size;
    }
}
