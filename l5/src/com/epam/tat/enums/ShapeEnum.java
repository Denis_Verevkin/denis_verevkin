package com.epam.tat.enums;

/**
 * Created by dverevkin on 7/5/2017.
 */
public enum ShapeEnum {

    CIRCLE("circle"),
    TRIANGLE("triangle"),
    RECTANGLE("rectangle"),
    SQUARE("square");

    private String name;

    ShapeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static ShapeEnum getByName(String name) {
        for (ShapeEnum shapeEnum : ShapeEnum.values()) {
            if (shapeEnum.getName().equals(name)) {
                return shapeEnum;
            }
        }
        return null;
    }
}
