package com.epam.tat;

import com.epam.tat.enums.ShapeEnum;
import com.epam.tat.shapes.*;

public class Main {

    public static void main(String[] args) {
        // triangle:3 circle:6 square:4 rectangle:5:6 square:16 triangle:5
        if (args != null && args.length > 0) {
            for (String arg : args) {
                String[] splitArgs = arg.split(":");
                ShapeEnum shapeEnum = ShapeEnum.getByName(splitArgs[0]);
                if (shapeEnum != null) {
                    if (splitArgs.length > 2) {
                        System.out.println(shapeEnum.getName() + " - " + splitArgs[1] + ", " + splitArgs[2]);
                    } else {
                        System.out.println(shapeEnum.getName() + " - " + splitArgs[1]);
                    }
                    Shape shape = null;
                    switch (shapeEnum) {
                        case CIRCLE:
                            shape = new Circle(Integer.parseInt(splitArgs[1]));
                            break;
                        case TRIANGLE:
                            shape = new Triangle(Integer.parseInt(splitArgs[1]));
                            break;
                        case RECTANGLE:
                            shape = new Rectangle(Integer.parseInt(splitArgs[1]), Integer.parseInt(splitArgs[2]));
                            break;
                        case SQUARE:
                            shape = new Square(Integer.parseInt(splitArgs[1]));
                            break;
                    }
                    if (shape != null) {
                        System.out.println("Area:" + shape.area());
                        System.out.println("Perimeter:" + shape.perimeter());
                    }
                }
            }
        }
    }
}
